# Auto Catalog

The simple CRUD for Auto Catalog

## Usage

```bash
composer install
cd src/App/
php index.php
```
Then type your command and enjoy :)

## Configuration

You can change to debugging mode, editing src/App/Config/App.php file

## Running tests

```bash
cd src/App/
php index.php test
```

## To import data using JSON, use the following template, or see the template in import.json file in the root folder

```json
[
	{
		"licensePlateNumber": "a",
		"brand": "b",
		"color": "c",
		"horsePower": 123
	},
	{
		"licensePlateNumber": "a2",
		"brand": "b2",
		"color": "c2",
		"horsePower": 243
	}
]
```


## License
[MIT](https://choosealicense.com/licenses/mit/)