<?php

namespace App;

/**
 * Class ClientInterface
 * @package App\ClientInterface
 */
class ClientInterface
{
    /** @var App */
    private $app;

    /**
     * ClientInterface constructor.
     *
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @param null|string $command
     */
    public function initCommandExecutor(?string $command = null): void
    {
        if (!$command) {
            $command = readline("\nPlease type one of the following commands:\n\n create\n search\n total\n delete\n import\n\n");
        }

        $this->app->initialize($command)->run();
        $this->initCommandExecutor();
    }
}
