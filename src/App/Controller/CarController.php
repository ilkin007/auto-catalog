<?php

namespace App\Controller;

use App\View\CliView;
use App\Car\CarBuilder;
use App\Car\CarCollection;

/**
 * Class CarController
 */
class CarController
{
    /** @var CarBuilder */
    private $carBuilder;

    /** @var CarCollection */
    private $carCollection;

    /** @var CliView */
    private $cliView;

    /**
     * CarController constructor.
     *
     * @param CarBuilder $carBuilder
     * @param CarCollection $carCollection
     * @param CliView $cliView
     */
    public function __construct(CarBuilder $carBuilder, CarCollection $carCollection, CliView $cliView)
    {
        $this->carBuilder = $carBuilder;
        $this->carCollection = $carCollection;
        $this->cliView = $cliView;
    }

    /**
     * @param array $params
     *
     * @return void
     * @throws \Exception
     */
    public function addCar(
        array $params
    ): void {
        $car = $this->carBuilder->build($params);
        $this->carCollection->offsetSet($params[ 'licensePlateNumber' ], $car);
    }

    /**
     * @param string $keyword
     *
     * @throws \Exception
     */
    public function search(string $keyword): void
    {
        $car = $this->carCollection->search($keyword);

        $this->cliView->printCar($car);
    }

    public function printTotal()
    {
        $message = $this->carCollection->getTotal();
        $this->cliView->printTotal($message);
    }

    /**
     * @param string $licensePlateNumber
     *
     * @throws \Exception
     */
    public function delete(string $licensePlateNumber): void
    {
        $this->carCollection->offsetUnset($licensePlateNumber);
    }

    /**
     * @param string $jsonContent
     *
     * @return int
     * @throws \Exception
     */
    public function import(string $jsonContent): int
    {
        $array = json_decode($jsonContent);

        foreach ($array as $key => $value) {
            $value = (array)$value;
            $newCar = $this->carBuilder->build($value);
            $this->carCollection->addEntity($newCar);
        }

        return count($array);
    }

    /**
     * @param string $message
     */
    public function printSuccess(string $message): void
    {
        $this->cliView->printSuccess($message);
    }

    /**
     * @param string $message
     */
    public function printError(string $message): void
    {
        $this->cliView->printError($message);
    }
}
