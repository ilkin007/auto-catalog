<?php

return [
    'debug' => true,
    'storage' => 'memory' //memory, file or database, can be extended
];