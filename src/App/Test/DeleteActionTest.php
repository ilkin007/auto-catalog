<?php

namespace App\Test;

/**
 * Class TotalActionTest
 * @package App\Test
 */
class DeleteActionTest extends CarDataProvider
{
    public function testTotalCar()
    {
        static::assertEquals(3, $this->carCollection->getTotal());
    }

    public function testCanDeleteCar()
    {
        try {
            $licensePlateNumber = '10-DD-106';
            $this->carCollection->offsetUnset($licensePlateNumber);

            static::assertFalse($this->carCollection->offsetExists($licensePlateNumber));
        } catch (\Exception $e) {
            static::fail();
        }
    }
}