<?php

namespace App\Test;

use App\Entity\Car;
use App\Entity\CarBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class CarTest
 * @package App\Test
 */
class CarTest extends TestCase
{
    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();
        $carBuilder->createCar();
        $carBuilder->addBrand('Mercedes-Benz');
        $carBuilder->addColor('white');
        $carBuilder->addHorsePower(142);
        $carBuilder->addLicensePlateNumber('90-DN-016');

        $this->assertInstanceOf(Car::class, $carBuilder->getCar());
    }
}
