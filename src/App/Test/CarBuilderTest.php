<?php

namespace App\Test;

use Exception;
use App\Car\Car;
use App\Car\CarBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class CarBuilderTest
 * @package App\Test
 */
class CarBuilderTest extends TestCase
{
    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();

        $carParams = [
            'licensePlateNumber' => '90-DN-016',
            'brand'              => 'Mercedes-Benz',
            'color'              => 'white',
            'horsePower'         => 142
        ];

        try {
            $car = $carBuilder->build($carParams);
        } catch (Exception $e) {
            self::fail();
        }

        static::assertInstanceOf(Car::class, $car);
    }

    /**
     * @expectedException Exception
     */
    public function testThrowsExceptionWhileBuildCar()
    {
        $carBuilder = new CarBuilder();

        $carWrongParams = [
            'AAAlicensePlateNumber' => '90-DN-016',
            'Abrand'                => 'Mercedes-Benz',
            'Acolor'                => 'white',
            'AhorsePower'           => 142
        ];

        $this->expectException(Exception::class);

        $carBuilder->build($carWrongParams);
    }
}
