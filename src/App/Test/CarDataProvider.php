<?php

namespace App\Test;

use App\Car\CarBuilder;
use App\Car\CarCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class CarDataProvider
 * @package App\Test
 */
class CarDataProvider extends TestCase
{
    /** @var CarCollection */
    protected $carCollection;

    //Create few cars for testing purposes
    public function setUp(): void
    {
        parent::setUp();

        $carBuilder = new CarBuilder();
        $this->carCollection = new CarCollection();

        $cars = [
            [
                'licensePlateNumber' => '90-DN-016',
                'brand'              => 'Mercedes-Benz',
                'color'              => 'white',
                'horsePower'         => 142
            ],
            [
                'licensePlateNumber' => '10-DD-100',
                'brand'              => 'BMW',
                'color'              => 'black',
                'horsePower'         => 255
            ],
            [
                'licensePlateNumber' => '10-DD-106',
                'brand'              => 'Volkswagen',
                'color'              => 'yellow',
                'horsePower'         => 140
            ]
        ];

        try {
            foreach ($cars as $car) {
                $carEntity = $carBuilder->build($car);
                $this->carCollection->addEntity($carEntity);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();

            exit;
        }
    }
}
