<?php

namespace App\Test;

/**
 * Class TotalActionTest
 * @package App\Test
 */
class TotalActionTest extends CarDataProvider
{
    public function testTotalCar()
    {
        static::assertEquals(3, $this->carCollection->getTotal());
    }
}