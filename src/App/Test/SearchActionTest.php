<?php

namespace App\Test;

use App\Exception\ElementNotFoundException;

/**
 * Class CarBuilderTest
 * @package App\Test
 */
class SearchActionTest extends CarDataProvider
{
    /** @expectedException */
    public function testCanNotFindACar()
    {
        $this->expectException(ElementNotFoundException::class);
        $this->carCollection->search('90-DD-155');
    }

    public function testCanFindACar()
    {
        try {
            $licensePlateNumber = '10-DD-100';
            $car = $this->carCollection->search($licensePlateNumber);
            static::assertEquals($licensePlateNumber, $car->getLicensePlateNumber());
        } catch (\Exception $e) {
            static::fail();
        }
    }
}
