<?php

namespace App\Controller;

use App\Entity\CarBuilder;
use App\Collection\CarCollection;
use App\Adaptor\StorageAdaptorContract;
use Exception;

/**
 * Class CarPresenter
 */
class CarPresenter
{
    /** @var CarBuilder */
    private $carBuilder;

    /** @var StorageAdaptorContract */
    private $storageAdaptor;

    /**
     * CarPresenter constructor.
     * @param CarBuilder $carBuilder
     * @param StorageAdaptorContract $storageAdaptor
     */
    public function __construct(CarBuilder $carBuilder, StorageAdaptorContract $storageAdaptor)
    {
        $this->carBuilder = $carBuilder;
        $this->storageAdaptor = $storageAdaptor;
    }

    /**
     * @param CarCollection $carCollection
     * @param array $params
     * @return void
     * @throws \Exception
     */
    public function addCar(
        CarCollection $carCollection,
        array $params
    ): void {
        $car = $this->carBuilder->build($params);
        $carCollection->offsetSet($params['licensePlateNumber'], $car);
        $this->storageAdaptor->store('carCollection', serialize($carCollection));

        var_dump($this->storageAdaptor->read('carCollection'));

        exit;
    }
    
    /**
     * @return CarCollection
     * @throws Exception
     */
    public function loadCarCollection(): CarCollection
    {
        $carCollection = unserialize($this->storageAdaptor->read('carCollection'));

        if (!$carCollection instanceof CarCollection) {
            throw new Exception("Can't read data from the storage!");
        }

        return $carCollection;
    }
}
