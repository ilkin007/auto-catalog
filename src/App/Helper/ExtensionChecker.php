<?php

namespace App\Helper;

use Exception;

/**
 * Class ExtensionChecker
 * @package App\Helper
 */
class ExtensionChecker
{
    /**
     * @param string $fileName
     * @param array $extensions
     *
     * @throws Exception
     */
    public function checkWhetherFileHasExtension(string $fileName, array $extensions): void
    {
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        $allowedExtensionsList = '';
        foreach ($extensions as $extension) {
            $allowedExtensionsList .= $extension . ' or ';
        }

        $allowedExtensionsList = substr($allowedExtensionsList, 0, -4);

        if (!in_array($ext, $extensions)) {
            throw new Exception("Please check the file extension, allowed extension(s): " . $allowedExtensionsList);
        }
    }
}