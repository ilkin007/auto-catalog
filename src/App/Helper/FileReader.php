<?php

namespace App\Helper;

/**
 * Class FileReader
 * @package App\Adaptor
 */
class FileReader
{
    private $filePath;

    /**
     * FileReader constructor.
     *
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getFileContents(): string
    {
        if (!$fileContents = file_get_contents($this->filePath)) {
            throw new \Exception("Can't reach the file... Please check the path");
        }

        return $fileContents;
    }
}