<?php

namespace App\Helper;

use Exception;

/**
 * Class JSONImporter
 * @package App\Adaptor
 */
class JSONImporter
{
    /**
     * @param string $string
     *
     * @return string
     * @throws Exception
     */
    public function getContents(string $string): string
    {
        json_decode($string);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception("Please check your file, the JSON syntax is invalid!");
        }

        return $string;
    }
}