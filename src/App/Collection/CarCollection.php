<?php

namespace App\Collection;

use ArrayAccess;
use App\Entity\Car;
use Exception;

/**
 * Class CarCollection
 */
class CarCollection implements ArrayAccess
{
    private $array = [];

    /**
     * @param Car $car
     */
    public function addEntity(Car $car): void
    {
        $licensePlateNumber = $car->getLicensPlateNumber();
        $this->array[$licensePlateNumber] = $car;
    }

    /**
     * We're able to find Car object by any of parameters
     * @param string $keyword
     * @return Car|null
     */
    private function findEntity(string $keyword): ?Car
    {
        /* @var Car $car */
        foreach ($this->array as $car) {
            if (in_array($keyword, $car->getAllAttributesList(), false)) {
                return $car;
            }
        }

        return null;
    }

    /**
     * return int
     */
    public function count()
    {
        return count($this->array);
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($licensePlateNumber): bool
    {
        return array_key_exists($licensePlateNumber, $this->array);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($licensePlateNumber): Car
    {
        return $this->array[$licensePlateNumber];
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function offsetSet($licensePlateNumber, $car): void
    {
        if ($this->offsetExists($licensePlateNumber)) {
            throw new Exception("The car with this license plate already exists!");
        }

        $this->array[$licensePlateNumber] = $car;
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($licensePlate): void
    {
        unset($this->array[$licensePlate]);
    }

//    /**
//     * @return string
//     */
//    public function toJson(): string
//    {
//        return json_encode($this->serializeArrayElements());
//    }
//
//    /**
//     * @return array
//     */
//    private function serializeArrayElements(): array
//    {
//        $serializedArray = [];
//        /** @var Car $car */
//        foreach ($this->array as $car) {
//            $arrayToEncode[] = serialize($car);
//        }
//
//        return $serializedArray;
//    }
}