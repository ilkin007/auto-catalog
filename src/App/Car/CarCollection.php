<?php

namespace App\Car;

use ArrayAccess;
use App\Exception\ElementNotFoundException;
use App\Exception\ElementAlreadyExistsException;

/**
 * Class CarCollection
 */
class CarCollection implements ArrayAccess
{
    private $array = [];

    /**
     * @param Car $car
     */
    public function addEntity(Car $car): void
    {
        $licensePlateNumber = $car->getLicensePlateNumber();
        $this->array[ $licensePlateNumber ] = $car;
    }

    /**
     * We're able to find Car object by any of parameters
     *
     * @param string $keyword
     *
     * @return Car
     * @throws ElementNotFoundException
     */
    public function search(string $keyword): Car
    {
        /* @var Car $car */
        foreach ($this->array as $car) {
            if (in_array($keyword, $car->getAllAttributesList(), false)) {
                if ($this->offsetExists($car->getLicensePlateNumber())) {
                    return $car;
                }
            }
        }

        throw new ElementNotFoundException("Car was not found...");
    }

    /**
     * return int
     */
    public function getTotal(): int
    {
        return count($this->array);
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($licensePlateNumber): bool
    {
        return array_key_exists($licensePlateNumber, $this->array);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($licensePlateNumber): Car
    {
        return $this->array[ $licensePlateNumber ];
    }

    /**
     * @inheritDoc
     * @throws ElementAlreadyExistsException
     */
    public function offsetSet($licensePlateNumber, $car): void
    {
        if ($this->offsetExists($licensePlateNumber)) {
            throw new ElementAlreadyExistsException("The car with this license plate already exists!");
        }

        $this->array[ $licensePlateNumber ] = $car;
    }

    /**
     * @inheritDoc
     * @throws ElementNotFoundException
     */
    public function offsetUnset($licensePlateNumber): void
    {
        if (!$this->offsetExists($licensePlateNumber)) {
            throw new ElementNotFoundException("Car was not found!");
        }

        unset($this->array[ $licensePlateNumber ]);
    }
}