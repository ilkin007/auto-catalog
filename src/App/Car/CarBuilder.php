<?php

namespace App\Car;

/**
 * Class CarBuilder
 * @package App\Entity
 */
class CarBuilder implements CarBuilderContract
{
    /** @var Car */
    private $car;

    /**
     * @param array $inputParams
     *
     * @return Car
     * @throws \Exception
     */
    public function build(array $inputParams): Car
    {
        $this->car = new Car();

        $requiredParams = $this->car->getAllAttributesList();

        //Check if we have right parameters to initialize an object
        if (count(array_diff_key($requiredParams, $inputParams))) {
            throw new \Exception("Sorry, parameters don't match...\n\n");
        }

        $this->car->setLicensePlateNumber($inputParams[ 'licensePlateNumber' ]);
        $this->car->setBrand($inputParams[ 'brand' ]);
        $this->car->setColor($inputParams[ 'color' ]);
        $this->car->setHorsePower($inputParams[ 'horsePower' ]);

        return $this->car;
    }
}