<?php

namespace App\Car;

/**
 * Interface CarBuilderContract
 * @package App\Contract
 */
Interface CarBuilderContract
{
    /**
     * @param array $params
     * @return Car
     */
    public function build(array $params): Car;
}