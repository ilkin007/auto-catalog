<?php

namespace App\Car;

/**
 * Class Car
 */
class Car implements CarContract
{
    /**
     * @var string
     */
    private $licensePlateNumber;

    /**
     * @var string
     */
    private $color;

    /**
     * @var float
     */
    private $horsePower;

    /**
     * @var string
     */
    private $brand;

    /**
     * @return string
     */
    public function getLicensePlateNumber(): string
    {
        return $this->licensePlateNumber;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @return float
     */
    public function getHorsePower(): float
    {
        return $this->horsePower;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return array
     */
    public function getAllAttributesList(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param string $licensePlateNumber
     */
    public function setLicensePlateNumber(string $licensePlateNumber): void
    {
        $this->licensePlateNumber = $licensePlateNumber;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @param float $horsePower
     */
    public function setHorsePower(float $horsePower): void
    {
        $this->horsePower = $horsePower;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }


}