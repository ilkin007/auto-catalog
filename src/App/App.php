<?php

namespace App;

use App\Controller\CarController;
use App\Action\TotalAction;
use App\Action\CreateAction;
use App\Action\DeleteAction;
use App\Action\SearchAction;
use App\Action\ActionContract;
use App\Action\ImportJSONAction;
use App\Action\WrongCommandAction;

/**
 * Class App
 * @package App\Router
 */
final class App
{
    private $carController;

    private $actions = [
        'search'       => SearchAction::class,
        'create'       => CreateAction::class,
        'delete'       => DeleteAction::class,
        'import'   => ImportJSONAction::class,
        'total'        => TotalAction::class,
        'wrongCommand' => WrongCommandAction::class
    ];

    /**
     * AbstractAction constructor.
     *
     * @param CarController $carController
     */
    public function __construct(CarController $carController)
    {
        $this->carController = $carController;
    }

    /**
     * @param string $action
     *
     * @return ActionContract
     */
    public function initialize(string $action): ActionContract
    {
        if (!key_exists($action, $this->actions)) {
            return new WrongCommandAction($this->carController);
        }

        return new $this->actions[ $action ]($this->carController);
    }
}
