<?php

namespace App\Adaptor;


/**
 * Class MemoryStorageAdaptor
 * @package App\Adaptor
 */
class MemoryStorageAdaptor implements StorageAdaptorContract
{
    /** @inheritDoc */
    public function store(string $key, string $value): bool
    {
        return (bool)apc_store($key, $value);
    }

    /** @inheritDoc */
    public function read(string $key): string
    {
        return apc_fetch($key);
    }
}
