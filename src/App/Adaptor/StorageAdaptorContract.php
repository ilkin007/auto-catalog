<?php

namespace App\Adaptor;

/**
 * Interface StorageAdaptorContract
 * @package App\Contract
 */
Interface StorageAdaptorContract
{
    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function store(string $key, string $value): bool;

    /**
     * @param string $key
     * @return string
     */
    public function read(string $key): string;
}