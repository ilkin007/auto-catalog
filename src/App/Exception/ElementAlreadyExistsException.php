<?php

namespace App\Exception;

use Exception;

/**
 * Class ElementAlreadyExistsException
 * @package App\Exception
 */
class ElementAlreadyExistsException extends Exception
{

}