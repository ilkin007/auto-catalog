<?php

namespace App\Exception;

use Exception;

/**
 * Class ElementNotFoundException
 * @package App\Exception
 */
class ElementNotFoundException extends Exception
{

}