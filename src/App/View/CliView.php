<?php

namespace App\View;

use App\Car\Car;

/**
 * Class CliView
 */
class CliView
{
    /**
     * @param Car $car
     *
     */
    public function printCar(Car $car): void
    {
        $decoratedCar = '[0;32;40m';
        $carDetails = $car->getAllAttributesList();

        foreach ($carDetails as $key => $carDetail) {
            $decoratedCar .= $key . ': ' . $carDetail . "\n------------\n";
        }

        echo $decoratedCar . "\e[0m\n\n";
    }

    /**
     * @param int $amount
     *
     * @return void
     */
    public function printTotal(int $amount): void
    {
        echo "Total amount of cars: " . $amount . "\n\n";
    }

    /**
     * @param string $message
     */
    public function printError(string $message): void
    {
        echo "\e[0;31;40m\n" . $message . "\e[0m\n";
    }

    /**
     * @param string $message
     */
    public function printSuccess(string $message): void
    {
        echo "\e[0;32;40m\n" . $message . "\e[0m\n";
    }

    /**
     * @param string $message
     */
    public function printPlain(string $message): void
    {
        echo $message;
    }
}