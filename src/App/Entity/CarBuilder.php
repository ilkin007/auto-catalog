<?php

namespace App\Entity;

use App\Contract\CarBuilderContract;

/**
 * Class CarBuilder
 * @package App\Entity
 */
class CarBuilder implements CarBuilderContract
{
    /** @var Car */
    private $car;

    /**
     * @param array $params
     * @return Car
     */
    public function build(array $params): Car
    {
        $this->car = new Car();

        $this->car->setLicensePlateNumber($params['licensePlateNumber']);
        $this->car->setBrand($params['brand']);
        $this->car->setColor($params['color']);
        $this->car->setHorsePower($params['horsePower']);

        return $this->car;
    }
}