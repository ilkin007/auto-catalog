<?php

namespace App\Action;

/**
 * Class WrongCommandAction
 * @package App\ClientInterface\Actions
 */
class WrongCommandAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        $this->carController->printError("Command not found...");
    }
}
