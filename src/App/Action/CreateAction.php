<?php

namespace App\Action;

use Exception;

/**
 * Class CreateAction
 * @package App\ClientInterface\Actions
 */
class CreateAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        if (!$params) {
            $params[ 'licensePlateNumber' ] = readline("License Plate Number (ex. BE-10-404):\n\n");
            $params[ 'brand' ] = readline("Brand (ex. Volkswagen):\n\n");
            $params[ 'color' ] = readline("Color (ex. yellow):\n\n");
            $params[ 'horsePower' ] = (float)readline("Horse Power (ex. 144.5):\n\n");
        }

        try {
            $this->carController->addCar($params);
            $this->carController->printSuccess("The car has been successfully created!");
        } catch (Exception $e) {
            $this->carController->printError($e->getMessage());
        }
    }
}
