<?php

namespace App\Action;

/**
 * Interface ActionContract
 * @package App\ClientInterface\Actions
 */
interface ActionContract
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void;
}