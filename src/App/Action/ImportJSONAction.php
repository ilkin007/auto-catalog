<?php

namespace App\Action;

use Exception;
use App\Helper\FileReader;
use App\Helper\JSONImporter;
use App\Helper\ExtensionChecker;

/**
 * Class SearchAction
 * @package App\ClientInterface\Actions
 */
class ImportJSONAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        $filePath = readline("Please type a full path of a JSON file path to import:\n\n");
        $extensionChecker = new ExtensionChecker();
        $jsonImporter = new JSONImporter();
        $fileReader = new FileReader($filePath);

        try {
            $extensionChecker->checkWhetherFileHasExtension($filePath, ['json']);
            $jsonContent = $jsonImporter->getContents($fileReader->getFileContents());

            $amountAdded = $this->carController->import($jsonContent);

            $this->carController->printSuccess("{$amountAdded} car(s) have been successfully imported!");
        } catch (Exception $e) {
            $this->carController->printError($e->getMessage());
        }
    }
}
