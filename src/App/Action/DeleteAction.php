<?php

namespace App\Action;

use Exception;

/**
 * Class DeleteAction
 * @package App\ClientInterface\Actions
 */
class DeleteAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        if (!$params) {
            $params[ 'licensePlateNumber' ] = readline("Please type license Plate Number(ex . BE - 10 - 404):\n\n");
        }

        try {
            $this->carController->delete($params[ 'licensePlateNumber' ]);
            $this->carController->printSuccess("The car with License Plate Number {$params['licensePlateNumber']} has been successfully deleted");
        } catch (Exception $e) {
            $this->carController->printError($e->getMessage());
        }
    }
}
