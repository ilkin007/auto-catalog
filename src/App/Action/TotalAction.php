<?php

namespace App\Action;

/**
 * Class TotalAction
 * @package App\ClientInterface\Actions
 */
class TotalAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        $this->carController->printTotal();
    }
}
