<?php

namespace App\Action;

use Exception;

/**
 * Class SearchAction
 * @package App\ClientInterface\Actions
 */
class SearchAction extends AbstractAction
{
    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {
        if(!$params){
            $params['keyword'] = readline("Please type a search keyword:");
        }

        try {
            $this->carController->search($params['keyword']);
        } catch (Exception $e) {
            $this->carController->printError($e->getMessage());
        }
    }
}
