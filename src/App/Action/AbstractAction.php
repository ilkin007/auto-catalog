<?php

namespace App\Action;

use App\Controller\CarController;

/**
 * Class AbstractAction
 * @package App\ClientInterface\Actions
 */
class AbstractAction implements ActionContract
{
    protected $carController;

    /**
     * AbstractAction constructor.
     *
     * @param CarController $carController
     */
    public function __construct(CarController $carController)
    {
        $this->carController = $carController;
    }

    /**
     * @param array|null $params
     */
    public function run(?array $params = null): void
    {

    }
}