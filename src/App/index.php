<?php

use App\Entity\CarBuilder;
use App\Controller\CarPresenter;
use App\Adaptor\MemoryStorageAdaptor;

//Checking if it run using CLI and stop execution if not
if (php_sapi_name() !== 'cli') {
    exit;
}

$config = require __DIR__ . '/Config/App.php';

if ($config['debug']) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

if ($config['storage'] === 'memory') {
    $storageAdaptor = new MemoryStorageAdaptor();
}

function runTests()
{
    echo shell_exec('./phpunit src/App/Tests');

    exit;
}

require __DIR__ . '/../../vendor/autoload.php';

//In case client wants to run tests
if (isset($argv[1]) && $argv[1] === 'test') {
    runTests();
}

$carPresenter = new CarPresenter(new CarBuilder(), $storageAdaptor);

try {
    $carCollection = $carPresenter->loadCarCollection();
} catch (Exception $e) {
    echo $e->getMessage();

    exit;
}


$command = $argv[1];

switch ($command) {
    case 'search':
        $searchKeyword = readline("Please type a search keyword:\n\n");

        break;
    case 'total':

        break;
    case 'create':
        $params = [];
        $params['licensePlateNumber'] = readline("License Plate Number (ex. BE-10-404):\n\n");
        $params['brand'] = readline("Brand (ex. Volkswagen):\n\n");
        $params['color'] = readline("Color (ex. yellow):\n\n");
        $params['horsePower'] = (float)readline("Horse Power (ex. 144.5):\n\n");

        try {
            $carPresenter->addCar($carCollection, $params);
        } catch (Exception $e) {
            echo $e->getMessage();

            exit;
        }

        echo 'The car has been successfully created!';

        break;
    case 'delete':

        break;
    case 'import':

        break;
}
