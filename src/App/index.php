<?php

use App\App;
use App\View\CliView;
use App\Car\CarBuilder;
use App\ClientInterface;
use App\Car\CarCollection;
use App\Controller\CarController;

//Checking if it run using CLI and stop execution if not
if (php_sapi_name() !== 'cli') {
    exit;
}

require __DIR__ . '/../../vendor/autoload.php';
$config = require __DIR__ . '/Config/App.php';

if ($config[ 'debug' ]) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

function runTests()
{
    echo shell_exec('../../phpunit .');
    exit;
}

//In case client wants to run tests
if (isset($argv[ 1 ]) && $argv[ 1 ] === 'test') {
    runTests();
}

$carController = new CarController(new CarBuilder(), new CarCollection(), new CliView);
$app = new App($carController);
$clientInterface = new ClientInterface($app);

$clientInterface->initCommandExecutor();
